せつめい {{{
========
	* 原本リンク  
	  原本リンクは、サムネの生成元ファイルに繋がっている。

}}}

もくじ {{{
============
* [キャラ](#キャラ)
	* [複数キャラ](#複数キャラ)
	* [祝園アカネ](#祝園アカネ)
	* [山家宏佳](#山家宏佳)
	* [垂水結菜](#垂水結菜)
	* [少佐](#少佐)
	* [英賀保芽衣](#英賀保芽衣)
	* [中舟生良文](#中舟生良文)
	* [パキラ](#パキラ)
	* [葛城岩男](#葛城岩男)
	* [モブ姉(KITS受付嬢)](#モブ姉kits受付嬢)
	* [夢前さくら](#夢前さくら)
	* [御来屋なんとか + カメラマン](#御来屋なんとか-カメラマン)
	* [万能倉まな](#万能倉まな)
	* [京姫鉄道マスコット](#京姫鉄道マスコット)
	* [余部静夫](#余部静夫)
	* [敬川康](#敬川康)
	* [朝霧よしみちゅ](#朝霧よしみちゅ)
	* [篠山ささみん](#篠山ささみん)
* [場所設定・絵素材](#場所設定絵素材)
	* [京鉄本社](#京鉄本社)
	* [KITS](#kits)
	* [駅](#駅)
	* [指令管制室](#指令管制室)
* [物設定・絵素材](#物設定絵素材)
	* [物](#物)
	* [鉄道車両](#鉄道車両)
* [京鉄設定・絵素材](#京鉄設定絵素材)
	* [一般](#一般)
	* [ロゴ等](#ロゴ等)
	* [制服](#制服)
* [KITS設定・絵素材](#kits設定絵素材)
	* [ロゴ等](#ロゴ等)
* [各種テンプレ](#各種テンプレ)
	* [作画担当者向け](#作画担当者向け)
	* [演出担当者向け](#演出担当者向け)
	* [撮影担当者向け](#撮影担当者向け)
}}}

キャラ {{{
======
複数キャラ {{{
----------

* 主要キャラ一覧図
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/_all.jpg)
	* [原本jpg](./designs/character/main-characters/_all.jpg)

* 第二話全キャラ一覧図
	* ![サムネjpg](./matomeSmallImg/designs/character/_all.jpg)
	* [原本png](./designs/character/_all.png)
	* [原本psd](./designs/character/_all.psd)

* 第二話全キャラ大きさ対比図
	* ![サムネjpg](./matomeSmallImg/designs/character/scale.jpg)
	* [原本png](./designs/character/scale.png)
	* [原本psd](./designs/character/scale.psd)
}}}

祝園アカネ {{{
----------
* 祝園アカネキャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/akane-hosono.jpg)
	* [原本psd](./designs/character/main-characters/akane-hosono.psd)
	* [原本png](./designs/character/main-characters/akane-hosono.png)

* 祝園アカネ表情パターン
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/akane-hosono_face-pattern.jpg)
	* [原本psd](./designs/character/main-characters/akane-hosono_face-pattern.psd)
	* [原本png](./designs/character/main-characters/akane-hosono_face-pattern.png)

* (再掲)祝園アカネ社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/akane_hosono.jpg)
	* [原本png](./designs/uniform/ID-card/akane_hosono.png)

* (再掲)祝園アカネ名札
	* ![サムネjpg](./matomeSmallImg/designs/uniform/nameplate/nameplate_housono.jpg)
	* [原本png](./designs/uniform/nameplate/nameplate_housono.png)

* (再掲)京鉄女性用制服(夏)基本設定
	* ![サムネjpg](./matomeSmallImg/designs/uniform/uniform-summer-female.jpg)
	* [原本jpg](./designs/uniform/uniform-summer-female.jpg)
	}}}

山家宏佳 {{{
--------
* 山家宏佳キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/hiroka-yamaga.jpg)
	* [原本psd](./designs/character/main-characters/hiroka-yamaga.psd)
	* [原本png](./designs/character/main-characters/hiroka-yamaga.png)

* (再掲)山家宏佳社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/hiroka_yamaga.jpg)
	* [原本png](./designs/uniform/ID-card/hiroka_yamaga.png)

* (再掲)制服右胸ロゴプレート
	* ![サムネjpg](./matomeSmallImg/designs/uniform/logoplate/logoplate.jpg)
	* [原本ai](./designs/uniform/logoplate/logoplate.ai)
	* [原本png](./designs/uniform/logoplate/logoplate.png)

* (再掲)山家宏佳名札
	* ![サムネjpg](./matomeSmallImg/designs/uniform/nameplate/nameplate_yamaga.jpg)
	* [原本ai](./designs/uniform/nameplate/nameplate.ai)
	* [原本png](./designs/uniform/nameplate/nameplate_yamaga.png)

* (再掲)京鉄制服設定1
	* ![サムネjpg](./matomeSmallImg/designs/uniform/uniform001.jpg)
	* [原本jpg](./designs/uniform/uniform001.jpg)

* (再掲)京鉄制服設定2
	* ![サムネjpg](./matomeSmallImg/designs/uniform/uniform-color.jpg)
	* [原本jpg](./designs/uniform/uniform-color.jpg)
	}}}

垂水結菜 {{{
--------
* 垂水結菜キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/yuina-tarumi.jpg)
	* [原本png](./designs/character/main-characters/yuina-tarumi.png)
	* [原本psd](./designs/character/main-characters/yuina-tarumi.psd)

* (再掲)垂水結菜社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/yuina_tarumi.jpg)
	* [原本png](./designs/uniform/ID-card/yuina_tarumi.png)

* (再掲)垂水結菜車掌名札
	* ![サムネjpg](./matomeSmallImg/designs/uniform/nameplate/nameplate_conductor.jpg)
	* [原本ai](./designs/uniform/nameplate/nameplate_conductor.ai)
}}}

少佐 {{{
----
* 少佐キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/lt-commander-shousa.jpg)
	* [原本png](./designs/character/main-characters/lt-commander-shousa.png)
	* [原本psd](./designs/character/main-characters/lt-commander-shousa.psd)

* 少佐表情パターン
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/lt-commander-shousa-hyoujo.jpg)
	* [原本psd](./designs/character/main-characters/lt-commander-shousa-hyoujo.psd)

* (再掲)少佐社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/LtCommander.jpg)
	* [原本png](./designs/uniform/ID-card/LtCommander.png)

* (再掲)京鉄徽章(帽子・制服のマーク)
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/logo/京姫鐵道_帽章.jpg)
	* [原本ai](./designs/kyoki-railway-company/logo/京姫鐵道_帽章.ai)
	* [原本png](./designs/kyoki-railway-company/logo/京姫鐵道_帽章.png)
}}}

英賀保芽衣 {{{
----------
* 英賀保芽衣キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/mei-agaho.jpg)
	* [原本psd](./designs/character/main-characters/mei-agaho.psd)
	* [原本png](./designs/character/main-characters/mei-agaho.png)

* (再掲)英賀保芽衣社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/mei_agaho.jpg)
	* [原本png](./designs/uniform/ID-card/mei_agaho.png)
* 英賀保芽衣服装通勤用
	* ![サムネjpg](./designs/character/main-characters/mei-agaho-tsukinyou.jpg)
	* [原本psd](./designs/character/main-characters/mei-agaho-tsukinyou.psd)
}}}

中舟生良文 {{{
----------
* CIO(中舟生良文)キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/yoshifumi_nakahunyu_CIO.jpg)
	* [原本png](./designs/character/main-characters/yoshifumi_nakahunyu_CIO.png)
	* [原本psd](./designs/character/main-characters/yoshifumi_nakahunyu_CIO.psd)

* (再掲)中舟生良文(CIO)社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/CIO_yoshifumi_nakafunyu.jpg)
	* [原本png](./designs/uniform/ID-card/CIO_yoshifumi_nakafunyu.png)
}}}

パキラ {{{
------
* パキラ(汎用型自走式パキラ)汎用素材1
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/pachira.jpg)
	* [原本psd](./designs/character/main-characters/pachira.psd)

* パキラ(汎用型自走式パキラ)汎用素材2
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/pachira_size.jpg)
	* [原本png](./designs/character/main-characters/pachira_size.png)

* パキラ(汎用型自走式パキラ)キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/main-characters/pachira_pattern.jpg)
	* [原本png](./designs/character/main-characters/pachira_pattern.png)
	}}}

葛城岩男 {{{
--------
* 葛城岩男キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/katsuragi.jpg)
	* [原本psd](./designs/character/sub-characters/katsuragi.psd)
}}}

モブ姉(KITS受付嬢) {{{
------------------
* モブ姉(KITS受付嬢)キャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/mob_female.jpg)
	* [原本psd](./designs/character/sub-characters/mob_female.psd)
}}}

夢前さくら {{{
----------
* 夢前さくらキャラデザ
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/yumesaki.jpg)
	* [原本psd](./designs/character/sub-characters/yumesaki.psd)
}}}

御来屋なんとか + カメラマン{{{
---------------------------
* 御来屋なんとか + カメラマン
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/Mikuriya_and_Cameraman.jpg)
	* [原本psd](./designs/character/sub-characters/Mikuriya_and_Cameraman.psd)
}}}

万能倉まな{{{
----------
* 万能倉まな
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/万能倉まな.jpg)
	* [原本psd](./designs/character/sub-characters/万能倉まな.psd)
}}}

京姫鉄道マスコット{{{
------------------
* 京姫鉄道マスコット
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/京姫鉄道マスコット.jpg)
	* [原本psd](./designs/character/sub-characters/京姫鉄道マスコット.psd)
}}}

余部静夫{{{
--------
* 余部静夫(ラフ案)
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/余部静夫_Ver1.0_(ラフ案).jpg)
	* [原本png](./designs/character/sub-characters/余部静夫_Ver1.0_(ラフ案).png)

* 余部静夫(完成版)
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/余部静夫_Ver3.0_(完成版).jpg)
	* [原本psd](./designs/character/sub-characters/余部静夫_Ver3.0_(完成版).psd)
}}}

敬川康{{{
------
* 敬川康
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/敬川康.jpg)
	* [原本psd](./designs/character/sub-characters/敬川康.psd)
}}}

朝霧よしみちゅ{{{
--------------
* 朝霧義満カラー案1
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/朝霧義満カラー案1.jpg)
	* [原本jpg](./designs/character/sub-characters/朝霧義満カラー案1.jpg)

* 朝霧義満カラー案2
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/朝霧義満カラー案2バリエーション.jpg)
	* [原本jpg](./designs/character/sub-characters/朝霧義満カラー案1バリエーション.jpg)

* 朝霧義満カラー設定案
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/朝霧義満カラー設定案.jpg)
	* [原本jpg](./designs/character/sub-characters/朝霧義満カラー設定案.jpg)

* 朝霧義満ラフ案
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/朝霧義満ラフ案.jpg)
	* [原本jpg](./designs/character/sub-characters/朝霧義満ラフ案.jpg)
}}}

篠山ささみん{{{
------------
* 篠山砂沙美
	* ![サムネjpg](./matomeSmallImg/designs/character/sub-characters/篠山砂沙美_完成版.jpg)
	* [原本jpg](./designs/character/sub-characters/篠山砂沙美_完成版.psd)
}}}
}}}

場所設定・絵素材 {{{
================
京鉄本社 {{{
--------
* 京鉄本社ビル3Dモデル1
	* ![サムネjpg](./matomeSmallImg/designs/building/kyokirailway-himeji-main-building/3D/building15.jpg)
	* [原本blend](./designs/building/kyokirailway-himeji-main-building/3D/building15.blend)

* 京鉄本社ビル3Dモデル2
	* ![サムネjpg](./matomeSmallImg/designs/building/kyokirailway-himeji-main-building/3D/building16.jpg)
	* [原本blend](./designs/building/kyokirailway-himeji-main-building/3D/building16.blend)

* 京鉄本社サーバー室内配置図
	* ![サムネjpg](./matomeSmallImg/designs/room/server-room/server_room_layout.jpg)
	* [原本pdf](./designs/room/server-room/server_room_layout.pdf)
	* [原本vsdx](./designs/room/server-room/server_room_layout.vsdx)

* 京鉄本社サーバー室関連各種ファイル(3D等)
	* [ファイル多すぎてめんどくせえからここを見てくれい!(多分フォルダ行きリンクになってる)](./designs/room/server-room/3D/)

* 京鉄本社外観(実写)
	* ![サムネjpg](./matomeSmallImg/designs/room/system-office/external-appearance.jpg)
	* [原本jpg](./designs/room/system-office/external-appearance.JPG)

* システム課オフィス設定
	* [原本txt](./designs/room/system-office/memo.txt)

* システム課オフィス内配置図
	* ![サムネjpg](./matomeSmallImg/designs/room/system-office/room-layout.jpg)
	* [原本pdf](./designs/room/system-office/room-layout.pdf)
	* [原本vsdx](./designs/room/system-office/room-layout.vsdx)

* システム課オフィスラフ絵1
	* ![サムネjpg](./matomeSmallImg/designs/room/system-office/rough1.jpg)
	* [原本jpg](./designs/room/system-office/rough1.jpg)
	* [原本sai](./designs/room/system-office/rough1.sai)

* システム課オフィスラフ絵2
	* ![サムネjpg](./matomeSmallImg/designs/room/system-office/rough2.jpg)
	* [原本jpg](./designs/room/system-office/rough2.jpg)
	* [原本sai](./designs/room/system-office/rough2.sai)

* システム課オフィス窓外風景(実写)
	* ![サムネjpg](./matomeSmallImg/designs/room/system-office/view_from_window.jpg)
	* [原本jpg](./designs/room/system-office/view_from_window.JPG)
}}}

KITS {{{
----
* KITSオフィス3Dモデル
	* ![サムネjpg](./matomeSmallImg/designs/room/KITS-dev1-room/3D/KITS_office2014_08.jpg)
	* [原本blend](./designs/room/KITS-dev1-room/3D/KITS_office2014_08.blend)

* KITSオフィス内配置図
	* ![サムネjpg](./matomeSmallImg/designs/room/KITS-dev1-room/room-layout.jpg)
	* [原本jpg](./designs/room/KITS-dev1-room/room-layout.jpg)
	* [原本pdf](./designs/room/KITS-dev1-room/room-layout.pdf)
	* [原本vsdx](./designs/room/KITS-dev1-room/room-layout.vsdx)
}}}

駅 {{{
--
* 駅内案内表示1
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/sign-board/houmen.jpg)
	* [原本ai](./designs/kyoki-railway-company/sign-board/houmen.ai)

* 駅内案内表示2
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/sign-board/led_moji_1.jpg)
	* [原本ai](./designs/kyoki-railway-company/sign-board/led_moji_1.ai)

* 駅内案内表示3
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/sign-board/led_moji_2.jpg)
	* [原本](./designs/kyoki-railway-company/sign-board/led_moji_2.ai)

* 駅名看板(京鉄三田)
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/sign-board/station_name.jpg)
	* [原本ai](./designs/kyoki-railway-company/sign-board/station_name.ai)
	* [原本png](./designs/kyoki-railway-company/sign-board/station_name.png)

* 駅名看板(ふくすみ,縦)
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/sign-board/station_name_tate.jpg)
	* [原本ai](./designs/kyoki-railway-company/sign-board/station_name_tate.ai)

* 改札3D
	* ![サムネjpg](./matomeSmallImg/designs/machines/ticket_gates/ticketgate.jpg)
	* [原本blend](./matomeSmallImg/designs/machines/ticket_gates/ticketgate.blend)
}}}

指令管制室{{{
----------
* 指令管制室3D
	* ![サムネjpg](./matomeSmallImg/designs/room/control-room/Command and control center2.jpg)
	* [原本blend](./designs/room/control-room/Command and control center2.blend)

* 指令管制室見取り図
	* ![サムネjpg](./matomeSmallImg/designs/room/control-room/drawing.jpg)
	* [原本png](./designs/room/control-room/drawing.png)
	* [原本pdf](./designs/room/control-room/drawing.pdf)
	* [原本vsdx](./designs/room/control-room/drawing.vsdx)
}}}
}}}

物設定・絵素材 {{{
==============
物 {{{
--
* 電話機3Dモデル
	* ![サムネjpg](./matomeSmallImg/designs/props-design/TEL/telephone09.jpg)
	* [原本blend](./designs/props-design/TEL/telephone09.blend)

* 祝園アカネ社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/akane_hosono.jpg)
	* [原本png](./designs/uniform/ID-card/akane_hosono.png)

* 中舟生良文(CIO)社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/CIO_yoshifumi_nakafunyu.jpg)
	* [原本png](./designs/uniform/ID-card/CIO_yoshifumi_nakafunyu.png)

* 山家宏佳社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/hiroka_yamaga.jpg)
	* [原本png](./designs/uniform/ID-card/hiroka_yamaga.png)

* 社員証(IDカード)システム課員全部込み
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/ID_Cards.jpg)
	* [原本ai](./designs/uniform/ID-card/ID_Cards.ai)

* 少佐社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/LtCommander.jpg)
	* [原本png](./designs/uniform/ID-card/LtCommander.png)

* 英賀保芽衣社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/mei_agaho.jpg)
	* [原本png](./designs/uniform/ID-card/mei_agaho.png)

* 垂水結菜社員証(IDカード)
	* ![サムネjpg](./matomeSmallImg/designs/uniform/ID-card/yuina_tarumi.jpg)
	* [原本png](./designs/uniform/ID-card/yuina_tarumi.png)
}}}

鉄道車両{{{
--------
* KKR-K133帯色
	* ![サムネjpg](./matomeSmallImg/designs/train/KKR-K133/color-obi.jpg)
	* [原本ai](./designs/train/KKR-K133/color-obi.ai)

* KKR-K133前面
	* ![サムネjpg](./matomeSmallImg/designs/train/KKR-K133/front.jpg)
	* [原本psd](./designs/train/KKR-K133/front.psd)

* KKR-K133内部
	* ![サムネjpg](./matomeSmallImg/designs/train/KKR-K133/inside.jpg)
	* [原本psd](./designs/train/KKR-K133/inside.psd)

* KKR-K133側面
	* ![サムネjpg](./matomeSmallImg/designs/train/KKR-K133/train.jpg)
	* [原本ai](./designs/train/KKR-K133/train.ai)
	* [原本png](./designs/train/KKR-K133/train.png)

}}}
}}}

京鉄設定・絵素材 {{{
================
一般 {{{
----
* 京鉄社内ネットワーク図
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/LAN/京姫鐵道ネットワーク図.jpg)
	* [原本pdf](./designs/kyoki-railway-company/LAN/京姫鐵道ネットワーク図.pdf)
	* [原本vsdx](./designs/kyoki-railway-company/LAN/京姫鐵道ネットワーク図.vsdx)

* 京鉄全域路線図
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/route-map/full-routemap.jpg)
	* [原本ai](./designs/kyoki-railway-company/route-map/full-routemap.ai)
	* [原本png](./designs/kyoki-railway-company/route-map/full-routemap.png)
	* [原本svg](./designs/kyoki-railway-company/route-map/full-routemap.svg)

* 京鉄全域路線図(一部拡大)
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/route-map/full-routemap-thumb.jpg)
	* [原本png](./designs/kyoki-railway-company/route-map/full-routemap-thumb.png)

* 京鉄路線地図1(google map)
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/route-map/京姫鉄道(プレイスマップ).jpg)
	* [原本png](./designs/kyoki-railway-company/route-map/京姫鉄道(プレイスマップ).png)

* 京鉄kmlファイル(詳細不明)
	* [原本kml](./designs/kyoki-railway-company/route-map/京姫鉄道.kml)

* 京鉄路線地図2(google map)
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/route-map/京姫鉄道ルート(ver2).jpg)
	* [原本png](./designs/kyoki-railway-company/route-map/京姫鉄道ルート(ver2).png)

* 京鉄駅名一覧
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/route-map/京姫鉄道駅名一覧.jpg)
	* [原本png](./designs/kyoki-railway-company/route-map/京姫鉄道駅名一覧.png)
	* [原本xlsx](./designs/kyoki-railway-company/route-map/京姫鉄道駅名一覧.xlsx)
}}}

ロゴ等 {{{
------
* 京鉄労組(京鉄乗労)ロゴ
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/logo/jourou-logo.jpg)
	* [原本ai](./designs/kyoki-railway-company/logo/jourou-logo.ai)
	* [原本png](./designs/kyoki-railway-company/logo/jourou-logo.png)

* 京鉄ロゴ1
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/logo/kyoki-railway-logo01.jpg)
	* [原本png](./designs/kyoki-railway-company/logo/kyoki-railway-logo01.png)

* 京鉄ロゴ2
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/logo/KKR-logo.jpg)
	* [原本ai](./designs/kyoki-railway-company/logo/KKR-logo.ai)
	* [原本png](./designs/kyoki-railway-company/logo/KKR-logo.png)

* 京鉄ロゴ3
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/logo/logo_only.jpg)
	* [原本ai](./designs/kyoki-railway-company/logo/logo_only.ai)
	* [原本svg](./designs/kyoki-railway-company/logo/logo_only.svg)
}}}

制服 {{{
----
* 京鉄徽章(帽子・制服のマーク)
	* ![サムネjpg](./matomeSmallImg/designs/kyoki-railway-company/logo/京姫鐵道_帽章.jpg)
	* [原本ai](./designs/kyoki-railway-company/logo/京姫鐵道_帽章.ai)
	* [原本png](./designs/kyoki-railway-company/logo/京姫鐵道_帽章.png)

* 制服右胸ロゴプレート
	* ![サムネjpg](./matomeSmallImg/designs/uniform/logoplate/logoplate.jpg)
	* [原本ai](./designs/uniform/logoplate/logoplate.ai)
	* [原本png](./designs/uniform/logoplate/logoplate.png)

* 山家宏佳名札
	* ![サムネjpg](./matomeSmallImg/designs/uniform/nameplate/nameplate_yamaga.jpg)
	* [原本ai](./designs/uniform/nameplate/nameplate.ai)
	* [原本png](./designs/uniform/nameplate/nameplate_yamaga.png)

* 祝園アカネ名札
	* ![サムネjpg](./matomeSmallImg/designs/uniform/nameplate/nameplate_housono.jpg)
	* [原本png](./designs/uniform/nameplate/nameplate_housono.png)

* 垂水結菜車掌名札
	* ![サムネjpg](./matomeSmallImg/designs/uniform/nameplate/nameplate_conductor.jpg)
	* [原本ai](./designs/uniform/nameplate/nameplate_conductor.ai)

* 駅長助役帽子ラフ絵
	* ![サムネjpg](./matomeSmallImg/designs/uniform/cap_assistant_stationmaster.jpg)
	* [原本jpg](./designs/uniform/cap_assistant_stationmaster.jpg)

* 駅長帽子ラフ絵
	* ![サムネjpg](./matomeSmallImg/designs/uniform/cap_stationmaster.jpg)
	* [原本jpg](./designs/uniform/cap_stationmaster.jpg)

* 輸送主任帽子ラフ絵
	* ![サムネjpg](./matomeSmallImg/designs/uniform/cap_transportation_officer.jpg)
	* [原本jpg](./designs/uniform/cap_transportation_officer.jpg)

* 京鉄コート(外套)設定
	* ![サムネjpg](./matomeSmallImg/designs/uniform/coat.jpg)
	* [原本jpg](./designs/uniform/coat.jpg)

* 制服設定補記
	* [原本txt](./designs/uniform/memo.txt)

* 京鉄制服設定1
	* ![サムネjpg](./matomeSmallImg/designs/uniform/uniform001.jpg)
	* [原本jpg](./designs/uniform/uniform001.jpg)

* 京鉄制服設定2
	* ![サムネjpg](./matomeSmallImg/designs/uniform/uniform-color.jpg)
	* [原本jpg](./designs/uniform/uniform-color.jpg)

* 京鉄制服設定詳細
	* ![サムネjpg](./matomeSmallImg/designs/uniform/uniform-details.jpg)
	* [原本jpg](./designs/uniform/uniform-details.jpg)

* 京鉄女性用制服(夏)基本設定
	* ![サムネjpg](./matomeSmallImg/designs/uniform/uniform-summer-female.jpg)
	* [原本jpg](./designs/uniform/uniform-summer-female.jpg)

* 京鉄作業服設定
	* ![サムネjpg](./matomeSmallImg/designs/uniform/work_clothes.jpg)
	* [原本jpg](./designs/uniform/work_clothes.jpg)

* 京鉄作業服設定2
	* ![サムネjpg](./matomeSmallImg/designs/uniform/work_clothes_type2.jpg)
	* [原本jpg](./designs/uniform/work_clothes_type2.jpg)

}}}
}}}

KITS設定・絵素材 {{{
================
ロゴ等 {{{
------
* KITSロゴ
	* ![サムネjpg](./matomeSmallImg/designs/KITS/logo/kits-logo.jpg)
	* [原本ai](./designs/KITS/logo/kits-logo.ai)
	* [原本png](./designs/KITS/logo/kits-logo.png)

* KITSロゴ
	* ![サムネjpg](./matomeSmallImg/designs/KITS/logo/kits-logo.jpg)
	* [原本ai](./designs/KITS/logo/kits-logo.ai)

}}}
}}}

各種テンプレ {{{
============
作画担当者向け {{{
--------------
* 作画用ひな型psd
	* ![サムネjpg](./matomeSmallImg/template/sakuga/SXX_CXX.jpg)
	* [原本psd](./template/sakuga/SXX_CXX.psd)

* 作画用タイムシート
	* ![サムネjpg](./matomeSmallImg/template/timesheet/TimeSheet.jpg)
	* [原本png](./template/timesheet/TimeSheet.png)
	* [原本psd](./template/timesheet/TimeSheet.psd)
}}}

演出担当者向け {{{
--------------
* コンテ用紙
	* ![サムネjpg](./matomeSmallImg/template/conte/conte_sheet.jpg)
	* [原本pdf](./template/conte/conte_sheet.pdf)
	* [原本png](./template/conte/conte_sheet.png)
	* [原本xlsx](./template/conte/conte_sheet.xlsx)
}}}

撮影担当者向け {{{
--------------
* 撮影セット一式
	* [ファイル多すぎてめんどくせえからここを見てくれい!(フォルダ行きリンク)](./template/satuei/)
}}}
}}}

}}}
