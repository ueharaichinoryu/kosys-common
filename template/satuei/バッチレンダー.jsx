﻿var debug = {
properties: function (obj){
var list = [];
for (var item in obj){
    list.push(item + ": " + obj[item]);
}
alert(list.join("\n"));
}
}

var main = function (){
////////////////////////////////////////変数定義////////////////////////////////////////
var windowSize = [600,600];
var listboxSize = [200,200];
var listboxLocate = [0,0];
var fileOpenButtonSize = [100,20];
var fileOpenButtonLocate = [220,0];
var upMoveButtonSize = [100,20];
var upMoveButtonLocate = [220,30];
var bottomMoveButtonSize = [100,20];
var bottomMoveButtonLocate = [220,60];
var removeButtonSize = [100,20];
var removeButtonLocate = [220,90];
var runButtonSize = [100,20];
var runButtonLocate = [220,120];
var loadListButtonSize = [100,20];
var loadListButtonLocate = [220,150];
var saveListButtonSize = [100,20];
var saveListButtonLocate = [220,180];
var compNameExpressSize = [200,20];
var compNameExpressLocate = [340,0];
var compNameSize = [200,20];
var compNameLocate = [340,20];
var RSNameExpressSize = [200,20];
var RSNameExpressLocate = [340,50];
var RSNameSize = [200,20];
var RSNameLocate = [340,70];
var OMNameExpressSize = [200,20];
var OMNameExpressLocate = [340,100];
var OMNameSize = [200,20];
var OMNameLocate = [340,120];
var outputPathExpressSize = [200,20];
var outputPathExpressLocate = [340,150];
var outputPathSize = [200,20];
var outputPathLocate = [340,170];
var outputPathButtonSize = [30,20];
var outputPathButtonLocate = [540,170];
var sequenceCheckboxSize = [50,20];
var sequenceCheckboxLocate = [340,200];

var listboxItemsDictionary = {}; //filename: filepath
var listboxItems = [];
////////////////////////////////////////関数定義////////////////////////////////////////
var listAdd = function (list1, list2){
var listOut = [];
for (var i = 0; i < Math.max(list1.length, list2.length); i++){
    if (!list1[i]){
        list1[i] = 0;
    }
    if (!list2[i]){
        list2[i] = 0;
    }
    listOut[i] = list1[i] + list2[i];
}
return listOut;
}
////////////////////////////////////////関数定義////////////////////////////////////////
var itemPositionGen = function (size, locate){
return locate.concat(listAdd(size,locate));
}
////////////////////////////////////////関数定義////////////////////////////////////////
var pathToFilename = function (path){
return path.replace(/^.*\\/g,"");
}
////////////////////////////////////////関数定義////////////////////////////////////////
var renewListbox = function (listbox, array){
listbox.removeAll();
for (var i = 0; i < array.length; i++){
    listbox.add("item", array[i]);
}
return listbox;
}
////////////////////////////////////////ウィンドウ生成////////////////////////////////////////
var win = new Window("window", "バッチレンダー", [0,0].concat(windowSize), {resizable: true});
win.center();
var listbox = win.add("listbox", itemPositionGen(listboxSize, listboxLocate),{multiselect: true});
var fileOpenButton = win.add("button", itemPositionGen(fileOpenButtonSize, fileOpenButtonLocate), "add to queue");
var upMoveButton = win.add("button", itemPositionGen(upMoveButtonSize, upMoveButtonLocate), "↑");
var bottomMoveButton = win.add("button", itemPositionGen(bottomMoveButtonSize, bottomMoveButtonLocate), "↓");
var removeButton = win.add("button", itemPositionGen(removeButtonSize, removeButtonLocate), "remove");
var runButton = win.add("button", itemPositionGen(runButtonSize, runButtonLocate), "RUN!");
var loadListButton = win.add("button", itemPositionGen(loadListButtonSize, loadListButtonLocate), "load queue");
var saveListButton = win.add("button", itemPositionGen(saveListButtonSize, saveListButtonLocate), "save queue");
var compNameExpress = win.add("statictext", itemPositionGen(compNameExpressSize, compNameExpressLocate), "レンダリング対象コンポ名:");
var compNameObj = win.add("edittext", itemPositionGen(compNameSize, compNameLocate), "コンポ名");
var RSNameExpress = win.add("statictext", itemPositionGen(RSNameExpressSize, RSNameExpressLocate), "レンダリング設定名:");
var RSNameObj = win.add("edittext", itemPositionGen(RSNameSize, RSNameLocate), "レンダリング設定名");
var OMNameExpress = win.add("statictext", itemPositionGen(OMNameExpressSize, OMNameExpressLocate), "出力モジュール名:");
var OMNameObj = win.add("edittext", itemPositionGen(OMNameSize, OMNameLocate), "出力モジュール名");
var outputPathExpress = win.add("statictext", itemPositionGen(outputPathExpressSize, outputPathExpressLocate), "出力ディレクトリパス:");
var outputPathObj = win.add("edittext", itemPositionGen(outputPathSize, outputPathLocate), Folder.myDocuments.fsName);
var outputPathButton = win.add("button", itemPositionGen(outputPathButtonSize, outputPathButtonLocate), "参照");
var sequenceCheckbox = win.add("checkbox", itemPositionGen(sequenceCheckboxSize, sequenceCheckboxLocate), "連番");
win.show();
////////////////////////////////////////挙動記述////////////////////////////////////////
fileOpenButton.onClick = function (){
var input = true;
while(input = File.openDialog("ファイルを選択して!", "*.aep")){
    listbox.add("item", decodeURI(input.name));
    listboxItemsDictionary[decodeURI(input.name)] = input.fsName;
    listboxItems.push(decodeURI(input.name));
}
}


upMoveButton.onClick = function (){
for (var i = 0; i < listbox.items.length; i ++){
    if (listbox.items[i].selected){
        if (i > 0){
            var temp = {};
            temp = listboxItems[i - 1];
            listboxItems[i -1] = listboxItems[i];
            listboxItems[i] = temp;
        }
    }
}
listbox.removeAll();
for (var i = 0; i < listboxItems.length; i ++){
    listbox.add("item",listboxItems[i]);
}
}


bottomMoveButton.onClick = function (){
for (var i = 0; i < listbox.items.length; i ++){
    if (listbox.items[i].selected){
        if (i < listboxItems.length - 1){
            var temp = {};
            temp = listboxItems[i + 1];
            listboxItems[i +1] = listboxItems[i];
            listboxItems[i] = temp;
        }
    }
}
listbox = renewListbox(listbox, listboxItems);
}


removeButton.onClick = function (){
for (var i = 0;  i < listboxItems.length; i++){
    if (listbox.items[i].selected){
        listboxItemsDictionary[listboxItems[i]] = undefined;
        listboxItems.splice(i, 1);
        renewListbox(listbox, listboxItems);
    }
}
}


runButton.onClick = function (){
for (var i = 0; i < listboxItems.length; i ++){
var item = listboxItemsDictionary[listboxItems[i]];
var fileName = listboxItems[i].replace(/\.[^\.]+$/, "");
var cmd = "cmd.exe /c \"aerender ";
//cmd += "-reuse ";
cmd += "-comp \"" + compNameObj.text + "\" ";
cmd += "-RStemplate \"" + RSNameObj.text + "\" ";
cmd += "-OMtemplate \"" + OMNameObj.text + "\" ";
cmd += "-project \"" + listboxItemsDictionary[listboxItems[i]] + "\" ";
cmd += "-output \"" + outputPathObj.text + "\\" + fileName;
if (sequenceCheckbox.value){
    var dir = new Folder(outputPathObj.text + "\\" + fileName);
    if (!dir.exists){
        if (!dir.create()){
            alert("ディレクトリ生成失敗\nディレクトリパス: " + outputPathObj.text);
            return;
        }
    }
    cmd += "\\" + fileName + "_[######]\" ";
}else{
    cmd +=  "\" ";
}
system.callSystem(cmd);
//alert(cmd);
}
}

loadListButton.onClick = function (){
var file = {};
if (!(file = File.openDialog("読み込みファイルを指定せよ"))){
    alert("ファイルが指定されていない");
    return;
}
if (!file.open("r")){
    alert("ファイルを開けない");
    return;
}
var strArray = file.read().split("\n");
file.close();
listboxItems = [];
listboxItemsDictionary = {};
for (var i = 0; i < strArray.length; i ++){
    if (strArray[i].match(/^:\w+:\s*/)){
        switch (strArray[i].match(/^:\w+:/)[0]){
        case ":compName:":
            compNameObj.text = strArray[i].replace(/^:\w+:\s*/,"");
            break;
        case ":RSName:":
            RSNameObj.text = strArray[i].replace(/^:\w+:\s*/,"");
            break;
        case ":OMName:":
            OMNameObj.text = strArray[i].replace(/^:\w+:\s*/,"");
            break;
        case ":outputPath:":
            outputPathObj.text = strArray[i].replace(/^:\w+:\s*/,"");
            break;
        case ":sequenceCheckbox:":
            sequenceCheckbox.value = eval(strArray[i].replace(/^:\w+:/,""))
            break;
        }
        continue;
    }
    var fileName = pathToFilename(strArray[i]);
    listboxItems.push(fileName);
    listboxItemsDictionary[fileName] = strArray[i];

}
renewListbox (listbox, listboxItems);
}


saveListButton.onClick = function (){
var strArray = [];
strArray.push(":compName: " + compNameObj.text);
strArray.push(":RSName: " + RSNameObj.text);
strArray.push(":OMName: " + OMNameObj.text);
strArray.push(":outputPath: " + outputPathObj.text);
strArray.push(":sequenceCheckbox: " + sequenceCheckbox.value);

for (var i = 0; i < listboxItems.length; i ++){
    strArray.push(listboxItemsDictionary[listboxItems[i]]);
}
var fileName = "";
if (! (fileName = File.saveDialog("出力先リストファイルを指定せよ", "*.txt"))){
    return;
}
var file = new File(fileName);
if (file.exists){
    if (confirm("上書きしますか?")){
        file.open("w");
        file.write(strArray.join("\n"));
        file.close();
    }
}
} //end of saveListButton.onClick


outputPathButton.onClick = function (){
var dir = {};
if (!(dir = Folder.selectDialog("出力先ディレクトリを指定せよ", Folder.myDocuments))){
    return;
}
outputPathObj.text = dir.fsName;
}


} //end of main 




var test = function (){
alert(":aaabbb:cccddd".match(/^:\w+:/g));
}

main();