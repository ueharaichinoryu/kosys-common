﻿// vi: set foldmethod=marker # vimエディタ用モードライン

// 「こうしす」第二話撮影スクリプト
// 著者: 玉虫型偵察器
//
// 仮撮影説明:
// 仮撮影用ひな型aepにフォーマットに従ったレイヤー構造のpsdを読み込み、psdのルートコンポを選択しスクリプトを走らせるとpsd内の作画・BOOK・BGの各コンポをaep内に組み込む。
// コメント中の依存関数の記述は信用しないこと。

(function (){
var g = {
	//コンポツリー検索時検索対象レイヤー正規表現
	treeHanteiRegExp: /^[^#]/
}
///// makeAnimeCelComp()関係 /////
function changeLayerDuration(layer, durationFrame){
	//layerのinPointからの計算で尺をdurationFrameに設定する//{{{
	//引: layer: 対象レイヤーオブジェクト  durationFrame: レイヤーの尺
	//戻: 処理済レイヤーオブジェクト
	layer.outPoint = layer.inPoint + (layer.containingComp.frameDuration * durationFrame);
	return layer;
}//}}}
function sortLayer(comp, syoujun, sortFunc){
	// コンポ内レイヤーをソート//{{{
	// 引: ソート対象コンポ,昇順(下が大)か否かbool, ソート用判定関数(undef時文字コードソート)
	if (!(comp instanceof CompItem)){
		throw {name: "sortLayer", message: "第一引数がコンポアイテムでない"};
	}
	if (typeof syoujun != "boolean"){
		throw {name: "sortLayer", message: "第二引数がブール値でない"};
	}
	if (comp.layers.length < 2){return comp;}
	if (typeof sortFunc === "undefined"){
		function sortFunc(layerA, layerB){ // {{{
			if (layerA.name === layerB.name){
				return true;
			}else if (layerA.name.length > layerB.name.length){
				var charNum = layerB.name.length;
				var large = "a";
			}else{
				charNum = layerA.name.length;
				large = "b";
			}
			for (var j = 0 ; j < charNum; j++){
				if (layerA.name.charCodeAt(j) > layerB.name.charCodeAt(j)){
					return false;
				}else if (layerB.name.charCodeAt(j) > layerA.name.charCodeAt(j)){
					return true;
				}
			}
			// 短いほうのlayer名が長いほうの頭と全て一致している場合、長いほうが上
			if (large === "b"){
				return false;
			}else{
				return true;
			}
		} // }}}
	}
	if (syoujun){ // 昇順ソート
		var i = 1;
		while (i < comp.numLayers){
			if (!sortFunc(comp.layer(i), comp.layer(i + 1))){
				comp.layer(i + 1).moveBefore(comp.layer(i));
				if (i !== 1){i--;}
			}else{
				i++;
			}
		}
	}else{ // 降順ソート
		var i = 1;
		while (i < comp.numLayers){
			if (sortFunc(comp.layer(i), comp.layer(i + 1))){
				comp.layer(i + 1).moveBefore(comp.layer(i));
				if (i !== 1){i--;}
			}else{
				i++;
			}
		}
	}
}//}}}
function changeCompsLayerDulation(comp, durationFrame){
	// コンポ内全レイヤーの尺をdurationFrameに設定、ソート、表示(目玉をオン)//{{{
	// 引: コンポオブジェクト, 設定尺(フレーム単位)
	// 戻: 処理済コンポオブジェクト
	// 依存: sortLayer(), changeLayerDuration()
	sortLayer(comp,true);
	for (var i = 1; i < comp.layers.length + 1; i++){
		changeLayerDuration(comp.layers[i], durationFrame);
		comp.layers[i].enabled = true;
	}
	return comp;
}//}}}
function compsLayersSequence(comp){
	// コンポ内全レイヤーをシーケンス化//{{{
	// 引: コンポオブジェクト
	// 戻: 処理済コンポオブジェクト
	var nowLastTime = comp.layers[1].inPoint;
	for (var i = 1; i < comp.layers.length + 1; i++){
		comp.layers[i].startTime = nowLastTime - (comp.layers[i].inPoint - comp.layers[i].startTime)
		nowLastTime = comp.layers[i].outPoint;
	}
	return comp;
}//}}}
function makeAnimeTsComp(comp){
	//アニメ撮影用1フレシーケンスコンポ化//{{{
	//引: コンポオブジェクト
	//戻: 処理済コンポオブジェクト
	//依存: changeCompsLayerDulation(), compsLayersSequenc()
	if (comp.layers.length === 0){return comp;}
	changeCompsLayerDulation(comp, 1);
	comp.layers[1].startTime = 0 - (comp.layers[1].inPoint - comp.layers[1].startTime);
	compsLayersSequence(comp);
	return comp;
}//}}}
function scr_makeAnimeCelComp(){
	// 開いているコンポを1フレシーケンス化//{{{
	app.beginUndoGroup("スクリプト");
	var comp = app.project.activeItem;
	if (comp instanceof CompItem){
		makeAnimeTsComp(comp);
	}else{
		alert("対象コンポ選択がおかしー");
	}
	app.endUndoGroup();
}//}}}

///// searchLayerCopy()関係 /////
function getSelectedItems(){
	// プロジェクトパネルの選択中アイテムを配列で返す。何も選択されていなければnullを返す。//{{{
	// 引: 無
	// 戻: アイテムオブジェクト配列
	var items = app.project.items
	var selected = [];
	for (var i = 1; i <= items.length; i++){
		if (items[i].selected){
			selected.push(items[i]);
		}
	}
	return selected;
}//}}}
function getSelectedCompItems(){
	// プロジェクトパネルで選択中のコンポを配列で返す。//{{{
	// 引: 無し
	// 戻り: 選択中コンポ配列
	// 依存: getSelectedItems()
	var selected = getSelectedItems();
	var selectedComps = [];
	for(var i = 0; i < selected.length; i++){
		if (selected[i] instanceof CompItem){
			selectedComps.push(selected[i]);
		}
	}
	return selectedComps;
}//}}}
function getCompTreeLayers(rootComp, treeHanteiFunc){
	// 指定コンポ以下のレイヤーを再帰取得。treeHanteiFuncがfalseを返すレイヤー階層以下は検索より除外。treeHanteiFuncを指定しなければ全レイヤーを返す。//{{{
	// 引き: 検索のルートコンポ, 除外判定関数
	// 戻り: レイヤー配列
	// 依存: 
	if (!(rootComp instanceof CompItem)){
		throw {name: "getCompTreeLayers()", message: "ルートコンポがCompItemでない"};
	}
	if (treeHanteiFunc === undefined){
		treeHanteiFunc = function (){ // イエスマン関数である
			return true;
		}
	}
	var allLayers = [];
	function saikiGetLayers(comp){
		for (var i = 1; i <= comp.numLayers; i++){
			if (treeHanteiFunc(comp.layers[i])){
				allLayers.push(comp.layers[i]);
				if (comp.layers[i] instanceof AVLayer &&
				 comp.layers[i].source instanceof CompItem){
					saikiGetLayers(comp.layers[i].source);
				}
			}
		}
		return 0;
	}
	saikiGetLayers(rootComp);
	return allLayers;
}//}}}
function searchCompTree(rootComp, searchRegExp){
	// 指定コンポ以下の全レイヤーを再帰検索。searchRegExpにマッチするレイヤーを配列で返す。//{{{
	// 引き: 検索ルートコンポ, 主検索判定正規表現
	// 戻り: 検索該当レイヤー配列
	// 依存: getCompTreeLayers(), g.treeHanteiRegExp
	function makeTreeHanteiFunc(regexp){
		return function(layer){
			// クロージャでツリー検索評価関数を生成
			// ツリー検索評価関数
			// 引き: レイヤーオブジェクト
			// 戻り: 真偽
			if (layer.name.search(regexp) !== -1 ||
			 (layer instanceof AVLayer &&
			 layer.source.name.search(regexp) !== -1)){
				return true;
			}
			return false;
		};
	}
	function makeSearchHanteiFunc(regexp){
		// クロージャで検索評価関数を生成
		return function(layer){
			// 主検索評価関数。
			// 引き: レイヤーオブジェクト
			// 戻り: 真偽
			if (!(layer instanceof AVLayer ||
			 layer instanceof CameraLayer ||
			 layer instanceof LightLayer ||
			 layer instanceof ShapeLayer ||
			 layer instanceof TextLayer)){
				alert("評価関数への引数がおかしい" + layer.name);
				return null;
			}
			if (layer instanceof AVLayer &&
			 layer.source.name.search(regexp) !== -1){
				return true;
			}else if (layer.name.search(regexp) !== -1){
				return true;
			}else{
				return false;
			}
		};
	}
	var searchHanteiFunc = makeSearchHanteiFunc(searchRegExp);
	var treeHanteiFunc = makeTreeHanteiFunc(g.treeHanteiRegExp);
	var allLayers = getCompTreeLayers(rootComp, treeHanteiFunc);
	var matchedLayers = [];
	for (var i = 0; i < allLayers.length; i++){
		if (searchHanteiFunc(allLayers[i])){
			matchedLayers.push(allLayers[i]);
		}
	}
	return matchedLayers;
}//}}}
function searchLayerCopy(rootComp, resultComp, searchRegExp){
	// 指定コンポ以下を再帰検索しレイヤをresultCompにコピー //{{{
	// 引き: 検索ルートコンポ コピー先コンポ 検索正規表現 検索対象正規表現
	// 戻り: コピー先コンポ
	// 依存: searchCompTree(), g.treeHanteiRegExp
	var matchedLayers = searchCompTree(rootComp, searchRegExp);
	for (var i = 0; i < matchedLayers.length; i++){
		matchedLayers[i].copyToComp(resultComp);
	}
	return resultComp;
}//}}}
function scr_searchLayerCopy(){
	// 開いているコンポ以下を再帰検索しレイヤをプロジェクトパネル中の選択コンポにコピー コピー先コンポを選択していなければ「resultComp」を新規作成//{{{
	var input = "";
	while(input === ""){
		input = prompt("検索正規表現を入力してｹﾝｻｸｹﾝｻｸ!");
	}
	try {
	var searchRegExp = eval(input)
	}catch(err){
		var searchRegExp = 0;
	}
	if (!(searchRegExp instanceof RegExp)){
		alert("正規表現入力がおかしい");
		return ;
	}
	var resultComp = getSelectedCompItems();
	if (resultComp.length === 0){
		resultComp = app.project.rootFolder.items.addComp("_searchresult", 1920, 1080, 1, 20, 24);
	}else{
			resultComp = resultComp[0];
	}
	var rootComp = app.project.activeItem;
	if (!(rootComp instanceof CompItem)){
		alert("検索基点コンポの選択がおかしー\n" + rootComp);
	}
	searchLayerCopy(rootComp, resultComp, searchRegExp);
	return 0;
}//}}}

///// 小関数群 /////
function layerCentering(layer){
	// レイヤーをセンタリング//{{{
	// キーフレームが無いもののみ
	// 引き: レイヤーオブジェクト
	// 戻り: レイヤーオブジェクト
	var compWidth = layer.containingComp.width;
	var compHeight = layer.containingComp.height;
	var property = layer("Transform")("Position");
	if (property.numKeys === 0){
		var value = property.valueAtTime(layer.startTime, false);
		value[0] = compWidth / 2;
		value[1] = compHeight / 2;
		property.setValue(value);
	}
	return layer;
}//}}}
function getCompTreeMaxLayerSize(comp){
	// コンポツリー内レイヤーの最大幅,高さを取得//{{{
	// 最大幅値と最大高さ値が別々のレイヤーから取得されることもあるので注意
	// 引き: コンポツリーの基点コンポ
	// 戻り: 最大レイヤーサイズの[x,y]2次元配列
	var allLayers = searchCompTree(comp, /.*/);
	var maxWidth = 0;
	var maxHeight = 0;
	for (var i = 0; i < allLayers.length; i++){
		if (allLayers[i].width > maxWidth){maxWidth = allLayers[i].width;}
		if (allLayers[i].height > maxHeight){maxHeight = allLayers[i].height;}
	}
	return [maxWidth, maxHeight];
}//}}}
function getCompTreeSourceItems(comp){
	// 指定コンポ以下レイヤーを再帰取得しソースアイテムを取得//{{{
	// 引き: 検索ルートコンポ, ツリー検索判定正規表現
	// 戻り: 使用されているアイテムの配列
	// 依存: searchCompTree()
	var layers = searchCompTree(comp, /.*/);
	var items = [];
	for (var i = 0; i < layers.length; i++){
		if (layers[i] instanceof AVLayer){
			items.push(layers[i].source);
		}
	}
	return items;
}//}}}
function changeCompsLayerBlend(comp, blendMode){
	// 指定コンポ内全レイヤーのブレンドモード変更//{{{
	// 引き: 対象コンポ, ブレンドモードオブジェクト(?)
	//   ブレンドモードオブジェクト(?)については↓参照
	//   https://sites.google.com/site/aescriptingreference/avlayer-object/attributes/blendingmode
	// 戻り: 対象コンポ
	for (var i = 1; i <= comp.numLayers; i++){
		if (comp.layers[i] instanceof AVLayer){
			comp.layers[i].blendingMode = blendMode;
		}
	}
}//}}}
function searchLayerItemCopy(rootComp, resultComp, searchRegExp){
	// 指定コンポ以下を再帰検索しレイヤをresultCompにコピー//{{{
	// 引き: 検索ルートコンポ コピー先コンポ 検索正規表現
	// 戻り: コピー先コンポ
	// 依存: searchCompTree(), 
	var matchedLayers = searchCompTree(rootComp, searchRegExp);
	for (var i = 0; i < matchedLayers.length; i++){
		if (matchedLayers[i] instanceof AVLayer){
			resultComp.layers.add(matchedLayers[i].source);
		}else{
			matchedLayers[i].copyToComp(resultComp);
		}
	}
	return resultComp;
}//}}}
function compChange(comp, newWidth, newHeight, newFrameRate){
	// 指定コンポ内コンポレイヤーの設定変更//{{{
	// レイヤー座標はサイズ変更に合わせて変えられる
	// フレームレート変更は任意
	// 引き: コンポ コンポ幅 コンポ高さ コンポフレームレート
	// 戻り: 処理済コンポ
	if (!(comp instanceof CompItem)){
		throw {name: "compChange()", message: "第一引数がコンポアイテムでない"};
	}
	var orgWidth = comp.width;
	var orgHeight = comp.height;
	var widthSabun = (newWidth - orgWidth) / 2;
	var heightSabun = (newHeight - orgHeight) / 2;
	comp.width = newWidth;
	comp.height = newHeight;
	if (newFrameRate !== undefined){
		comp.frameRate = newFrameRate;
	}
	for (var j = 1; j < comp.layers.length + 1; j++){
		var property = comp.layers[j]("Transform")("Position");
		if (property.numKeys === 0){
			var value = property.valueAtTime(comp.layers[j].startTime, false);
			value[0] += widthSabun;
			value[1] += heightSabun;
			property.setValue(value);
		}else{
			for (var k = 1; k <= property.numKeys; k++){
				value = property.keyValue(k);
				value[0] += widthSabun;
				value[1] += heightSabun;
				property.setValueAtKey(k, value);
			}
		}
	}
	return comp;
}//}}}
function saikiCompChange(comp, newWidth, newHeight, newFrameRate){
	// 指定コンポ以下再帰全コンポの設定変更//{{{
	// レイヤー座標はサイズ変更に合わせて変えられる
	// フレームレート変更は任意
	// 引き: 基準コンポ コンポ幅 コンポ高さ コンポフレームレート
	// 戻り: 処理済コンポアイテム配列
	// 依存: getCompTreeSourceItems()
	var sourceItems = getCompTreeSourceItems(comp);
	var compItems = [];
	for (var i = 0; i < sourceItems.length; i++){
		if (sourceItems[i] instanceof CompItem){
			compItems.push(compChange(sourceItems[i], newWidth, newHeight, newFrameRate));
		}
	}
	return compItems;
}//}}}
function saikiCollapse(comp, onOff){
	// 指定コンポ以下全プリコンレイヤー再帰コラップス操作//{{{
	// 引き: 基点コンポ, コラップスオンオフ(trueでオンに)
	// 戻り: 基点コンポ
	// 依存: getCompTreeLayers()
	function func(layer){
		if (layer.name.search(/^[^#]/) !== -1){
			return true;
		}else{
			return false;
		}
	}
	var layers = getCompTreeLayers(comp, func);
	for (var i = 0; i < layers.length; i++){
		if (layers[i].source instanceof CompItem){
			if (layers[i].collapseTransformation != onOff){
				layers[i].collapseTransformation = !layers[i].collapseTransformation;
			}
		}
	}
	return comp;
}//}}}
function addExpression(comp, expr){
	// 指定コンポ内全レイヤーにエクスプレッション適用//{{{
	for (var i = 1; i < comp.layers.length + 1; i++){
		comp.layers[i]("Transform")("Opacity").expression = expr;
	}
	return comp;
}//}}}
function copyLayers(comp,layers){
	// レイヤー群を指定コンポにコピー {{{
	// 引: コピー先Compオブジェクト, Layerオブジェクト配列
	// 戻り: コピー先Compオブジェクト
	if (!(comp instanceof AVLayer && comp.source instanceof CompItem)){
		throw {name: "copyLayers()", message: "第一引数がおかしい\n渡された第一引数:" + comp};
	}
	for (var i = 0; i < layers.length; i++){
		if (!(layers[i] instanceof Layer)){
			throw {name: "copyLayers()", message: 第二引数がおかしい};
		}
		layers[i].copyToComp(comp);
	}
	return comp;
} //}}}
function arraySearchRegexp(array, regexp){
	// 配列内を正規表現で検索 {{{
	// 引き: 配列, 検索正規表現
	// 戻り: 検索結果配列
	var result = [];
	for (var i = 0; i < array.length; i++){
		if (regexp.test(array[i])){
			result.push(array[i]);
		}
	}
	return result;
} // }}}
function arraySearchString(array, string){
	// 配列内を文字列で検索 {{{
	// 引き: 配列, 検索文字列
	// 戻り: マッチ時、最初のマッチ箇所インデックス、
	//       非マッチ時、-1
	for (var i = 0; i < array.length; i++){
		if (array[i] === string){
			return i;
		}
	}
	return -1;
} // }}}
function projectItemSearch(regexp){
	// プロジェクトアイテムを正規表現で検索 {{{
	// 引き: 検索正規表現
	// 戻り: 検索結果アイテム配列
	var result = [];
	for (var i = 1; i <= app.project.items.length; i++){
		if (regexp.test(app.project.items[i].name)){
			result.push(app.project.items[i]);
		}
	}
	return result;
} // }}}

///// こうしす第二話固有記述 /////
function scr_kousys2kari(parameter){
	// こうしす第二話スクリプトメインルーチン //{{{
	// タイムシート形式時は引数に"TS"、オーバーラップ形式時は"OL"を指定する。
	//
	if (parameter !== "TS" && parameter !== "OL"){
		throw {name:"kousys2()", message:"おかしな引数\n引数: " + parameter};
	}
	var sozaiRootComp = app.project.activeItem;
	var satueiRootComp = projectItemSearch(/1\.合成,制御\(仮\)/)[0];
	var regexp = /^([A-Z0-9])([0-9]+)(線画|塗り)(?:!([^\/]*))?(?:\/.*)?$/;
	var layers = searchCompTree(sozaiRootComp, regexp);
	var resultCelCompArray = [];
	var resultBgCompArray = [];
	var celLayerNameArray = [];
	var bgLayerNameArray = [];
	// コンポツリー全コンポサイズをツリー中レイヤーの最大サイズに合わせる
	// var maxXY = getCompTreeMaxLayerSize(sozaiRootComp);
	// saikiCompChange(sozaiRootComp, maxXY[0], maxXY[1], 24);
	//saikiCompChange(projectItemSearch(/4\.リサイズ\(仮\)/)[0], maxXY[0], maxXY[1], 24);
	saikiCompChange(projectItemSearch(/3\.カメラワーク\(仮\)/)[0], sozaiRootComp.width, sozaiRootComp.height, 24);
	var clearComps = projectItemSearch(/^[0-9A-Z](線|塗)素材\(仮\)$/); // カラにするコンポを取得
	for (var i = 0; i < clearComps.length; i++){
		var comp = clearComps.shift();
		if (comp.numLayers !== 0){
			while (comp.numLayers !== 0){
				comp.layers[1].remove();
			}
		}
	}
	for (i = 0; i < layers.length; i++){
		if (!(layers[i] instanceof AVLayer)){
			throw {name: "scr_kousys2kari2()", message: "AVLayer以外がかかったでゲソ\nレイヤー名: " + layers[i].name};
		}
		regexp.exec(layers[i].source.name);
		var layerName = {namae: RegExp.$1,
						 bangou: eval(RegExp.$2),
						 nurisen: RegExp.$3,
						 zokusei: RegExp.$4};
		if (layerName.nurisen == "線画"){
			var resultCompRegexp = eval("/^" + layerName.namae + "線素材\\(仮\\)" + "$/");
		} else if (layerName.nurisen == "塗り"){
			var resultCompRegexp = eval("/^" + layerName.namae + "塗素材\\(仮\\)" + "$/");
		}
		var resultComp = projectItemSearch(resultCompRegexp)[0];
		if (layerName.bangou === 1 && layerName.nurisen === "線画"){
			var resultPairCompRegexp = eval("/^" + layerName.namae + "塗素材\\(仮\\)" + "$/");
			var resultPairComp = projectItemSearch(resultPairCompRegexp)[0];
			if (arraySearchRegexp([layerName.zokusei.split(":")], /CEL/i).length != 0){
				projectItemSearch(eval("/^" + layerName.namae + "\\(仮\\)$/"))[0].layer(layerName.namae + "線素材(仮)").effect("BG")("チェックボックス").setValue(false);
				celLayerNameArray.push(layerName.namae);
				resultCelCompArray.push(resultComp);
				celLayerNameArray.push(layerName.namae);
				resultCelCompArray.push(resultPairComp);
			}else if (arraySearchRegexp([layerName.zokusei.split(":")], /BG/i).length != 0){
				projectItemSearch(eval("/^" + layerName.namae + "\\(仮\\)$/"))[0].layer(layerName.namae + "線素材(仮)").effect("BG")("チェックボックス").setValue(true);
				bgLayerNameArray.push(layerName.namae);
				resultBgCompArray.push(resultComp);
				resultBgCompArray.push(resultPairComp);
			}else if (arraySearchRegexp([layerName.zokusei.split(":")], /BOOK/i).length != 0){
				projectItemSearch(eval("/^" + layerName.namae + "\\(仮\\)$/"))[0].layer(layerName.namae + "線素材(仮)").effect("BG")("チェックボックス").setValue(true);
				bgLayerNameArray.push(layerName.namae);
				resultBgCompArray.push(resultComp);
				resultBgCompArray.push(resultPairComp);
			}
		}
		layers[i].copyToComp(resultComp);
	}
	// TSとOLの処理
	function makeExpression(layerNamae){
		return "//全レイヤーの不透明度にこのエクスプレッションを適用せよ。\n" +
"//これにより、「1.合成,制御(仮)」コンポ内当該レイヤーの「OL制御」エフェクトでの制御が可能となる。\n" +
"celName = \"" + layerNamae + "(仮)\";\n" +
"layerNumber = thisLayer.index * 100;\n" +
"OLControl = comp(\"1.合成,制御(仮)\").layer(celName).effect(\"OL制御\")(\"スライダー\");\n" +
"\n" +
"100 - Math.abs( layerNumber - OLControl);";
	}
	for (i = 0; i < bgLayerNameArray.length; i++){
		addExpression(resultBgCompArray[i * 2], makeExpression(bgLayerNameArray[i]));
		changeCompsLayerBlend(resultBgCompArray[i * 2], BlendingMode.ALPHA_ADD);
		addExpression(resultBgCompArray[i * 2 + 1], makeExpression(bgLayerNameArray[i]));
		changeCompsLayerBlend(resultBgCompArray[i * 2 + 1], BlendingMode.ALPHA_ADD);
	}
	if (parameter === "TS"){
		for (var i = 0; i < resultCelCompArray.length; i++){
			makeAnimeTsComp(resultCelCompArray[i]);
		}
		for (i = 0; i < resultBgCompArray.length; i++){
			sortLayer(resultBgCompArray[i], true);
		}
	}else if (parameter === "OL"){
		for (var i = 0; i < resultCelCompArray.length; i++){
			addExpression(resultCelCompArray[i], makeExpression(celLayerNameArray[i]));
			changeCompsLayerBlend(resultCelCompArray[i], BlendingMode.ALPHA_ADD);
			sortLayer(resultCelCompArray[i], true);
		}
		for (i = 0; i < resultBgCompArray.length; i++){
			sortLayer(resultBgCompArray[i], true);
		}
	}
}// }}}


/////kousys2prepare関係/////
function folderRecursiveGet(folder){
	// フォルダ内アイテムを再帰取得 {{{
	// 引: フォルダーアイテム
	// 戻: アイテム配列
	if (!(folder instanceof FolderItem)){
		throw {name: "folderRecursiveGet()", message: "引数がFolderItemでない"};
	}
	var items =[];
	items.push(folder);
	function recursive(folder){
		for (var i = 1; i <= folder.items.length; i++){
			if (folder.items[i] instanceof FolderItem){
				items.push(folder.items[i]);
				recursive(folder.items[i]);
			}else{
				items.push(folder.items[i]);
			}
		}
	}
	recursive(folder);
	return items;
} //}}}
function folderRecursiveRename(folder, arg1, arg2){
	// 選択中のフォルダ内の全アイテムの名前を変更 {{{
	// 引: 置換対象部分文字列又は正規表現, 置換後文字列
	// 戻: true
	if (!(folder instanceof FolderItem)){
		throw {name: "folderRecursiveRename()", message:"第一引数がフォルダーアイテムでない"};
	}
	var items = folderRecursiveGet(folder);
	for (var i = 0; i < items.length; i++){
		items[i].name = items[i].name.replace(arg1, arg2);
	}
	return true;
}// }}}
function listProperties(layer){
	// レイヤーのプロパティの配列を返す {{{
	// 引: レイヤー
	// 戻: プロパティオブジェクト配列
	if (!(layer instanceof AVLayer)){
		throw {name:"listEffectParameter()", message:"引数がAVLayerでない"};
	}
	var properties = [];
	function recursive(propertyG){
		for (var i = 1; i <= propertyG.numProperties; i++){
			if (propertyG.property(i) instanceof PropertyGroup){
				recursive(propertyG.property(i));
			}else (propertyG.property(i) instanceof Property){
				properties.push(propertyG.property(i));
			}
		}
	}
	recursive(layer);
	return properties;
} //}}}
function listExpressions(layer){
	// レイヤーのエクスプレッションを返す {{{
	// 引: AVLayer
	// 戻: {property: プロパティ名,expErr:エクスプレッションエラー(無ければ""), expression: エクスプレッション} の配列
	var properties =listProperties(layer);
	var expArray = [];
	for (var i = 0; i < properties.length; i++){
		if (properties[i].expression){
			expArray.push({
				property: properties[i].name,
				expErr: properties[i].expressionError,
				expression: properties[i].expression
			});
		}
	}
	return expArray;
} // }}}
function listFolderRecursiveExpressions(folder){
	// フォルダ内レイヤーの全エクスプレッションを返す {{{
	// 引: フォルダーアイテム
	// 戻: {comp: コンポ名, layer: レイヤー名, property: プロパティ名, expErr: エクスプレッションエラー(無ければ""), expression: エクスプレッション} の配列
	if (!(folder instanceof FolderItem)){
		throw {name: "listFolderRecursiveExpressions()", message: "引数がフォルダーアイテムでない"};
	}
	var items = folderRecursiveGet(folder);
	var exps = [];
	for (var i = 0; i < items.length; i++){
		if (items[i] instanceof CompItem){
			for (var j = 1; j <= items[i].numLayers; j++){
				if (items[i].layer(j) instanceof AVLayer){
					var listExpRet = listExpressions(items[i].layer(j));
					for (var k = 0; k < listExpRet.length; k++){
						exps.push({
							comp: items[i].name,
							layer: items[i].layer(j).name,
							property: listExpRet[k].property,
							expErr: listExpRet[k].expErr,
							expression:listExpRet[k].expression
						});
					}
				}
			}
		}
	}
	return exps;
} //}}}
function listFolderRecursiveExpressionsError(folder){
	// フォルダ内レイヤーの全エクスプレッションエラーテキストを返す {{{
	// 引: フォルダーアイテム
	// 戻: エラーとエスクプレッションのテキスト
	if (!(folder instanceof FolderItem)){
		throw({name: "listFolderRecursiveExpressionsError()", message: "引数がフォルダーアイテムでない"});
	}
	var objs = listFolderRecursiveExpressions(folder);
	var text = "";
	text += "フォルダー " + folder.name + " 内エクスプレッションエラー一覧:";
	text += "\n";
	text += "\n\n\n";
	for (var i = 0; i < objs.length; i++){
		if (objs[i].expErr != ""){
			text += "[エラー内容]: " + "\n";
			text += objs[i].expErr + "\n";
			text += "\n";
			text += "[エクスプレッション内容]:" + "\n";
			text += objs[i].expression + "\n";
			text += "\n";
			text += "\n";
			text += "====================" + "\n";
		}
	}
	return text;
} //}}}

function kousys2ModulePrepare(module, celName){
	// こうしす第二話セルモジュール前処理 {{{
	// セルモジュールコンポ群名前変更, 本撮コンポ群への統合
	// 引: セルモジュールのフォルダーアイテムオブジェクト, 置換先セル名1文字
	if (typeof celName != "string" || !/[0-9A-Z]/.test(celName)){
		throw {name: "kousys2prepare()", message: "引数が単一文字でない"};
	}
	if (!(module instanceof FolderItem)){
		throw {name: "kousys2prepare()", message: "フォルダーアイテムを選択していない"};
	}
	function addExpression(layer){
		var anchorPointExpression = 
"var firstChar = thisLayer.name.charAt(0);" + "\n" +
"var parentName = firstChar + \"(仮)\";" + "\n" +
"try{" + "\n" +
"	comp(\"1.合成,制御(仮)\").layer(parentName).transform.anchorPoint;" + "\n" +
 "}catch (e){" + "\n" +
"	transform.anchorPoint;" + "\n" +
"}";
		var positionExpression = 
"var firstChar = thisLayer.name.charAt(0);" + "\n" +
"var parentName = firstChar + \"(仮)\";" + "\n" +
"try{" + "\n" +
"comp(\"1.合成,制御(仮)\").layer(parentName).transform.position;" + "\n" +
"}catch (e){" + "\n" +
"transform.position;" + "\n" +
"}";
		var scaleExpression = 
"var firstChar = thisLayer.name.charAt(0);" + "\n" +
"var parentName = firstChar + \"(仮)\";" + "\n" +
"try{" + "\n" +
"comp(\"1.合成,制御(仮)\").layer(parentName).transform.scale;" + "\n" +
"}catch (e){" + "\n" +
"transform.scale;" + "\n" +
"}";
		var rotationExpression = 
"var firstChar = thisLayer.name.charAt(0);" + "\n" +
"var parentName = firstChar + \"(仮)\";" + "\n" +
"try{" + "\n" +
"comp(\"1.合成,制御(仮)\").layer(parentName).transform.rotation;" + "\n" +
"}catch (e){" + "\n" +
"transform.rotation;" + "\n" +
"}";
		var opacityExpression = 
"var firstChar = thisLayer.name.charAt(0);" + "\n" +
"var parentName = firstChar + \"(仮)\";" + "\n" +
"try{" + "\n" +
"	comp(\"1.合成,制御(仮)\").layer(parentName).transform.opacity;" + "\n" +
"}catch (e){" + "\n" +
"	transform.opacity;" + "\n" +
"}";
		layer("トランスフォーム")("アンカーポイント").expression = anchorPointExpression;
		layer("トランスフォーム")("位置").expression = positionExpression;
		layer("トランスフォーム")("スケール").expression = scaleExpression;
		layer("トランスフォーム")("回転").expression = rotationExpression;
		layer("トランスフォーム")("不透明度").expression = opacityExpression;
		return layer;
	}
	function mainProcess(){
		var from = new RegExp("^" + module.name.charAt(0));
		var to = celName;
		var celRootComp = {};
		var gouseiComp = {};
		var nuriTimeRemapComp = {};
		var nuriIntegratedTransformComp = {};
		var gaibuInputComp = {};
		var items = app.project.items;
		folderRecursiveRename(module, from, to);
		for (var i = 1; i <= items.length; i++){
			if (items[i] instanceof CompItem){
				if (items[i].name === celName){
					celRootComp = items[i];
				}else if (items[i].name === "-01:合成,処理"){
					gouseiComp = items[i];
				}else if (items[i].name === celName + "02cel:塗りタイムリマップ"){
					nuriTimeRemapComp = items[i];
				}else if (items[i].name === "-:塗り統合,トランスフォーム"){
					nuriIntegratedTransformComp = items[i];
				}else if (items[i].name === celName + ":色トレス用外部入力"){
					gaibuInputComp = items[i];
				}
			}
		}
		var flag = false;
		for (i = 1; i <= gouseiComp.numLayers; i++){
			if (gouseiComp.layers[i].name === celRootComp.name){
				flag = true; // レイヤー存在フラグ
			}
		}
		if (!flag){ // レイヤーがまだ存在していなかったら
			var layer = gouseiComp.layers.add(celRootComp);
			addExpression(layer);
		}
		sortLayer(gouseiComp, false);
		flag = false;
		for (i = 1; i <= nuriIntegratedTransformComp.numLayers; i++){
			if (nuriIntegratedTransformComp.layers[i].name === nuriTimeRemapComp.name){
				flag = true; // レイヤー存在フラグ
			}
		}
		if (!flag){ // レイヤーがまだ存在していなかったら
			layer = nuriIntegratedTransformComp.layers.add(nuriTimeRemapComp);
			addExpression(layer);
			gaibuInputComp.layers.add(nuriIntegratedTransformComp);
		}
		sortLayer(nuriIntegratedTransformComp, false);
	}
	mainProcess();

/* 本撮準備スクリプト仕様 {{{
// 「->」はレイヤー配置を意味する
レイヤ名変更, エクスプレッションエラー探査
A -> -:合成
	レイヤーにエクスプレッション付加 仮撮合成参照
A02cel:塗りタイムリマップ -> -:塗り統合, トランスフォーム
	レイヤーにエクスプレッション付加 仮撮合成参照
	-:塗り統合, トランスフォーム内ソート
-:塗り統合, トランスフォーム -> A:色トレス用外部入力

}}} */
} // }}}

function saikiFolderCompChange(folder, nW, nH, nFR){
	// 指定フォルダ以下再帰全コンポの設定変更//{{{
	// レイヤー座標はサイズ変更に合わせて変えられる
	// フレームレート変更は任意
	// 引き: 基準コンポ コンポ幅 コンポ高さ コンポフレームレート
	// 戻り: 処理済コンポアイテム配列
	// 依存: getCompTreeSourceItems()
	var sourceItems = folderRecursiveGet(folder);
	var compItems = [];
	for (var i = 0; i < sourceItems.length; i++){
		if (sourceItems[i] instanceof CompItem){
			compItems.push(sourceItems[i]);
		}
	}
	for (i = 0; i < compItems.length; i++){
		var orgWidth = compItems[i].width;
		var orgHeight = compItems[i].height;
		var widthSabun = (nW - orgWidth) / 2;
		var heightSabun = (nH - orgHeight) / 2;
		compItems[i].width = nW;
		compItems[i].height = nH;
		if (nFR !== undefined){
			compItems[i].frameRate = nFR;
		}
		for (var j = 1; j < compItems[i].layers.length + 1; j++){
			var property = compItems[i].layers[j]("Transform")("Position");
			if (property.numKeys === 0){
				var value = property.valueAtTime(compItems[i].layers[j].startTime, false);
				value[0] += widthSabun;
				value[1] += heightSabun;
				property.setValue(value);
			}else{
				for (var k = 1; k <= property.numKeys; k++){
					value = property.keyValue(k);
					value[0] += widthSabun;
					value[1] += heightSabun;
					property.setValueAtKey(k, value);
				}
			}
		}
	}
	return compItems;
}//}}}
function kousys2merge(kariFolder){
	// こうしす第二話仮撮本撮統合 {{{
	// 引:仮撮コンポフォルダ
	if (!(kariFolder instanceof FolderItem)){
		throw {name: "kousys2merge()", message: "フォルダーアイテムを選択していていない"};
	}
	var kariComps = [];
	var kariBGCheck = {}; // セル名 -> BGチェックボックスOnOffの連想配列
	var honCelSenMaterial = {}; // セル名 -> 本撮セル線素材コンポの連想配列
	var honCelNuriMaterial = {}; // セル名 -> 本撮セル塗素材コンポの連想配列
	var honBgSenMaterial = {}; // セル名 -> 本撮BG線素材コンポの連想配列
	var honBgNuriMaterial = {}; // セル名 -> 本撮BG塗素材コンポの連想配列
	var kariOutput = {}; // 仮撮aep outputコンポ
	var allItems = app.project.items;
	var kariItems = folderRecursiveGet(kariFolder);
	var xySize = []; // コンポサイズ
	var resizeRootComp = {}; // 統合系のリサイズルートコンポ
	var moduleRootFolder = {}; // 統合系のリサイズの基点フォルダ

	for (var i = 1; i <= allItems.length; i++){
		if (/^[0-9A-Z]01cel:線素材$/.test(allItems[i].name)){
			honCelSenMaterial[allItems[i].name.charAt(0)] = allItems[i];
		}else if (/^[0-9A-Z]02cel:塗素材$/.test(allItems[i].name)){
			honCelNuriMaterial[allItems[i].name.charAt(0)] = allItems[i];
		}else if(/^[0-9A-Z]01bg:線素材$/.test(allItems[i].name)){
			honBgSenMaterial[allItems[i].name.charAt(0)] = allItems[i];
		}else if(/^[0-9A-Z]02bg:塗素材$/.test(allItems[i].name)){
			honBgNuriMaterial[allItems[i].name.charAt(0)] = allItems[i];
		}else if (/^1\.合成,制御\(仮\)$/.test(allItems[i].name)){
			xySize = [allItems[i].width, allItems[i].height];
		}else if (/^-03:カメラワーク$/.test(allItems[i].name)){
			resizeRootComp = allItems[i];
		}else if (/^_output$/.test(allItems[i].name)){
			kariOutput = allItems[i];
		}else if (/^モジュール群$/.test(allItems[i].name)){
			moduleRootFolder = allItems[i];
		}
	}
	saikiFolderCompChange(moduleRootFolder, xySize[0], xySize[1]);
	saikiCompChange(resizeRootComp, xySize[0], xySize[1]);
	kariOutput.name = "_output(仮)";
	for (i = 0; i < kariItems.length; i++){
		if (kariItems[i] instanceof CompItem){
			kariComps.push(kariItems[i]);
			if (/^[0-9A-Z]\(仮\)$/.test(kariItems[i].name)){
				var fc = kariItems[i].name.charAt(0); // fc=first Char
				kariBGCheck[fc] = kariItems[i].layer(fc + "線素材(仮)")("エフェクト")("BG")("チェックボックス").value;
			}
		}
	}
	for (i = 0; i < kariComps.length; i++){
		if (/^[0-9A-Z]線素材\(仮\)$/.test(kariComps[i].name)){ // 線素材
			if (kariComps[i].numLayers !== 0){ 
				fc = kariComps[i].name.charAt(0);
				if (honCelSenMaterial[fc] === undefined){
					continue;
					throw {name:"kousys2merge", message:"該当モジュールが存在しない\n必要モジュール名: " + fc};
				}
				if (!kariBGCheck[fc]){ // セル線行き
					if (honCelSenMaterial[fc].numLayers !== 0){
						for (var j = 1; j <= honCelSenMaterial[fc].numLayers; j++){
							honCelSenMaterial[fc].layer(j).remove();
						}
					}
					honCelSenMaterial[fc].layers.add(kariComps[i]);
				}else{ // BG行き
					if (honBgSenMaterial[fc].numLayers !== 0){
						for (var j = 1; j <= honBgSenMaterial[fc].numLayers; j++){
							honBgSenMaterial[fc].layer(j).remove();
						}
					}
					honBgSenMaterial[fc].layers.add(kariComps[i]);
				}
			}
		}else if (/^[0-9A-Z]塗素材\(仮\)$/.test(kariComps[i].name)){// 塗り素材
			if (kariComps[i].numLayers !== 0){ 
				fc = kariComps[i].name.charAt(0);
				if (honCelNuriMaterial[fc] === undefined){
					continue;
					throw {name:"kousys2merge", message:"該当モジュールが存在しない\n必要モジュール名: " + fc};
				}
				if (!kariBGCheck[fc]){ // セル塗行き
					if (honCelNuriMaterial[fc].numLayers !== 0){
						for (var j = 1; j <= honCelNuriMaterial[fc].numLayers; j++){
							honCelNuriMaterial[fc].layer(j).remove();
						}
					}
					honCelNuriMaterial[fc].layers.add(kariComps[i]);
				}else{ // BG行き
					if (honBgNuriMaterial[fc].numLayers !== 0){
						for (var j = 1; j <= honBgNuriMaterial[fc].numLayers; j++){
							honBgNuriMaterial[fc].layer(j).remove();
						}
					}
					honBgNuriMaterial[fc].layers.add(kariComps[i]);
				}
			}
		}
	}

/* 仕様
function getCompTreeSourceItems(comp)
function saikiCompChange(comp, newWidth, newHeight, newFrameRate)
if (*線素材(仮).numLayers !== 0){
	if (comp("*(仮)").layer("*線素材(仮)")("エフェクト")("BG")("チェックボックス").value = 1){
		*線素材(仮) -> *01bg:線画
		*塗素材(仮) -> *01bg:塗り
	}else{
		*線素材(仮) -> *01cel:線画
		*塗素材(仮) -> *01cel:塗り
	}
}
	
*/ 
} // }}}
function stringUniqueTest(string){
	// stringの一意判定 {{{
	// 引: 判定対象string
	// 戻: 一意かどうかのbool
	for (var i = 0; i < string.length; i++){
		for (var j = i - 1; j >= 0; j--){
			if (string.charAt(i) === string.charAt(j)){
				return false;
			}
		}
	}
	return true;
} // }}}

//新規追加関数
function folderRecursiveSearch(folder, regexp, ret){
// フォルダーアイテムオブジェクト内再帰検索 {{{
// 引: フォルダーアイテムオブジェクト, 検索正規表現, リターン配列
// 戻: 検索マッチアイテム配列
	if (!(folder instanceof FolderItem)){
		throw {name: "folderRecursiveSearch", message: "第一引数がフォルダーアイテムオブジェクトでない"};
	}
	if (!(regexp instanceof RegExp)){
		throw {name: "folderRecursiveSearch", message: "第二引数が正規表現オブジェクトでない"};
	}
	if (!( ret instanceof Array)){
		var ret = [];
	}
	for (var i = 1; i <= folder.items.length; i++){
		if (regexp.test(folder.items[i].name)){
			ret.push(folder.items[i]);
		}
		if (folder.items[i] instanceof FolderItem){
			ret = ret.concat(folderRecursiveSearch(folder.items[i], regexp));
		}
	}
	return ret;
}// }}}

function searchUsingCel(kariFolder){
// 仮撮コンポ内で使用されているセル名一覧取得 {{{
// 引: 仮撮フォルダーアイテム
// 戻: セル名string("012AB"の形式)
//フォルダーアイテム内検索ループ
var items = folderRecursiveSearch(kariFolder, /(塗|線)素材\(仮\)/);
var matchedComps = [];
for (var i = 0; i < items.length; i++){
	if (items[i] instanceof CompItem && items[i].numLayers > 0){
		matchedComps.push(items[i]);
	}
}
var str = ""; // セル名一覧
for (i = 0; i < matchedComps.length; i++){
	var flag = false; // セル名一覧内に検査対象コンポのセル名があるかどうか
	for (var j = 0; j < str.length; j++){
		if (str.charAt(j) === matchedComps[i].name.charAt(0)){
			flag = true;
		}
	}
	if (!flag){
		str = str.concat(matchedComps[i].name.charAt(0));
	}
}
return str;
} // }}}

function kousys2main( kariAep, moduleAep){
	// こうしす第二話本撮メインルーチン
	// 引: 仮撮aepファイルオブジェクト, セルモジュールファイルオブジェクト
	// 戻:
	if (!(moduleAep instanceof File)){
		throw {name:"kousys2main()", message: "モジュールaepがファイルオブジェクトでない"};
	}
	if (!(kariAep instanceof File)){
		throw {name:"kousys2main()", message: "仮撮aepがファイルオブジェクトでない"};
	}
	var importOpts = new ImportOptions(kariAep);
	var kariFolder = app.project.importFile(importOpts); //kariFolder: 読み込まれた仮撮aepのルートのフォルダアイテムオブジェクト
	var cels = searchUsingCel(kariFolder);
	for (var i = 1; i <= app.project.rootFolder.numItems; i++){
		if (app.project.rootFolder.items[i].name === "モジュール群" && app.project.rootFolder.items[i] instanceof FolderItem){
			var moduleRootFolder = app.project.rootFolder.items[i];
		}
	}
	for (i = 0; i < cels.length; i++){
		importOpts = new ImportOptions(moduleAep);
		var moduleFolder = app.project.importFile(importOpts);
		moduleFolder.parentFolder = moduleRootFolder
		kousys2ModulePrepare(moduleFolder, cels.charAt(i));
	}
		kousys2merge(kariFolder);
}

function main(){
	app.beginUndoGroup("スクリプト");

	scr_kousys2kari("TS");
	//kousys2main(File.openDialog("仮撮aepを選択せよ", "*.aep"), File.openDialog("セルモジュールaepを選択せよ", "*.aep"));
	//alert(searchUsingCel(app.project.activeItem));

	app.endUndoGroup();

}

main();

})();
